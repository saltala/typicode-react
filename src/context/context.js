import React, { Component } from 'react';
import axios from 'axios';

const Context = React.createContext();

export class Provider extends Component {
    state = {
        users: [],
        user: {},
        loading: false,
        getUsers: async () => {
            this.setState({ loading: true });
            const users = await axios.get('https://jsonplaceholder.typicode.com/users');
            this.setState({ users: users.data, loading: false });
        },
        getUser: async (userId) => {
            this.setState({ loading: true });
            const user = await axios.get(`https://jsonplaceholder.typicode.com/users/${userId}`);
            this.setState({ user: user.data, loading: false })
        }
    }

    render() {
        return (
            <Context.Provider value={this.state}>
                {this.props.children}
            </Context.Provider>
        )
    }
}

export const Consumer = Context.Consumer;