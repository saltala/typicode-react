import React, { Component } from 'react'
import SearchInput from '../layout/SearchInput';
import CharacterList from '../character/CharacterList';
import rickmorty from '../../../../config/rickmorty';

class RickMortyHome extends Component {
    state = {
        searchValue: '',
        charResponseInfo: {},
        characters: [],
        error: '',
        loading: false,
    }

    setSearchValue = value => {
        this.setState({ searchValue: value });
        this.searchCharacters(value);
    }

    searchCharacters = async (value) => {
        this.setState({ loading: true, error: '' });
        try {
            const characters = await rickmorty.get(`/character/?name=${value}`);
            this.setState({ characters: characters.data.results, loading: false });
        } catch (err) {
            this.setState({ loading: false, error: `No se encontró personaje con ese nombre` })
        }
    }

    render() {
        return (
            <div>
                <h1 className="ui header">Busca un personaje</h1>
                <SearchInput setSearchValue={this.setSearchValue} />
                {this.state.error ? (<p>{this.state.error}</p>) : <CharacterList characters={this.state.characters} loading={this.state.loading} />}
            </div>
        )
    }
}

export default RickMortyHome;
