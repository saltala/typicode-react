import React from 'react'
import { Link } from 'react-router-dom';

const Pagination = (props) => {
    const { pages, numItems } = props;

    const renderPagination = () => {
        let pagination = [];

        for (let i = 1; i < pages; i++) {
            pagination.push(< Link to={`/rickandmorty/page/${i}`} className="item" >
                {i}
            </Link >)
        }

        return pagination;
    }

    return (
        <div className="ui pagination menu" style={{ marginTop: '1rem', marginBottom: '2rem' }}>
            {renderPagination()}
        </div>
    )
}


export default Pagination;