import React, { Component } from 'react'

class SearchInput extends Component {
    state = {
        searchTerm: '',
    }

    handleInputChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleFormSubmit = (event) => {
        event.preventDefault();

        if (this.state.searchTerm) {
            this.props.setSearchValue(this.state.searchTerm);
        }

        this.setState({ searchTerm: '' })
    }

    render() {
        return (
            <form className="ui form" onSubmit={this.handleFormSubmit} style={{ marginBottom: '2rem' }}>
                <div className="ui fluid icon input">
                    <input type="text" name="searchTerm" value={this.state.searchTerm} onChange={this.handleInputChange} placeholder="Busca personajes en el mundo de Rick & Morty..." />
                    <i className="search icon"></i>
                </div>
            </form>
        )
    }
}

export default SearchInput;