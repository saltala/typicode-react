import React, { Component, Fragment } from 'react';
import Character from './Character';
import Loader from '../../../typicode/layout/Loader';
import Pagination from '../layout/Pagination';

class CharacterList extends Component {
    renderCharacters = () => {
        if (this.props.loading) {
            return <Loader />
        } else {
            return (
                <React.Fragment>
                    {this.props.characters.map(character => (
                        <Character characterInfo={character} />
                    ))}
                </React.Fragment>
            )
        }
    }

    render() {
        return (
            <Fragment>
                <div className="ui grid">

                    {this.renderCharacters()}
                </div>
            </Fragment>
        )
    }
}

export default CharacterList;

//<Pagination pages={this.props.pages} numItems={this.props.numItems} />