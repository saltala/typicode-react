import React from 'react';
import { Link } from 'react-router-dom';

const Character = (props) => {
  const { id, name, status, species, type, gender, image, origin } = props.characterInfo;

  return (
    <div className="four wide column">
      <div className="ui card">
        <div className="image">
          <img src={image} alt="Character Avatar" />
        </div>
        <div className="content">
          <Link to={`rickandmorty/character/${id}`} className="header">{name}</Link>
          <div className="meta">
            <span className="date">{gender} - {origin.name}</span>
          </div>
          <div className="description">
            <ul>
              <li>Status: {status}</li>
              <li>Species: {species}</li>
              {type && <li>Type: {type}</li>}
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Character;