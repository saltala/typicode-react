import React, { Component } from 'react'
import axios from 'axios';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// Typicode
import Navbar from './typicode/layout/Navbar';
import UserList from './typicode/users/UserList';
import UserDetail from './typicode/users/UserDetail';
import PostDetails from './typicode/posts/PostDetails';

// Rick and Morty
import RickMortyHome from './rickmorty/components/pages/RickMortyHome';
import CharacterInfo from './rickmorty/components/character/CharacterInfo';

class App extends Component {
    state = {
        users: [],
        loading: false,
    };

    getUsers = async () => {
        this.setState({ loading: true });

        const users = await axios.get('https://jsonplaceholder.typicode.com/users');

        this.setState({ users: users.data, loading: false });
    }

    componentDidMount() {
        this.getUsers();
    }

    render() {
        const { users, loading } = this.state;

        return (
            <Router>
                <div className="App">
                    <Navbar />
                    <div className="ui container">
                        <Switch>
                            <Route exact path="/users" render={(routeProps) => (
                                <UserList {...routeProps} users={users} loading={loading} />
                            )} />
                            <Route exact path="/users/:id" render={(routeProps) => (
                                <UserDetail {...routeProps} />
                            )} />
                            <Route exact path="/users/:userId/posts/:postId" component={PostDetails} />
                            <Route exact path="/rickandmorty" component={RickMortyHome} />
                            <Route exact path="/rickandmorty/character/:id" component={CharacterInfo} />
                        </Switch>
                    </div>
                </div>
            </Router>
        )
    }
}

export default App;