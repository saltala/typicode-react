import React from 'react'
import { Link } from 'react-router-dom';

import NoImage from '../../../no-image.png';

const PostItem = (props) => {
    const { id, userId, title, body } = props.postInfo;

    return (
        <Link className="item" to={`/users/${userId}/posts/${id}`}>
            <div className="ui tiny image">
                <img src={NoImage} alt="Post" />
            </div>
            <div className="content">
                <div className="header">{title}</div>
                <div className="description">
                    <p>{body}</p>
                </div>
            </div>
        </Link>
    )
}

export default PostItem;
