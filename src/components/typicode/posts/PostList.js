import React from 'react'
import PostItem from './PostItem';

const PostList = ({ posts }) => {

    const renderPostItems = () => {
        return posts.map(post => (
            <PostItem key={post.id} postInfo={post} />
        ));
    }

    return (
        <React.Fragment>
            <h1>Posts</h1>
            <div className="ui link items">
                {renderPostItems()}
            </div>
        </React.Fragment>
    )
}

export default PostList;