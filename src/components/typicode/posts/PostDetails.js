import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

import Loader from '../layout/Loader';
import CommentList from '../comments/CommentList';

class PostDetails extends Component {
    state = {
        post: {},
        loading: false,
    }

    componentDidMount() {
        this.getPostInfo();
    }

    getPostInfo = async () => {
        this.setState({ loading: true });

        const postInfo = await axios.get(`https://jsonplaceholder.typicode.com/posts/${this.props.match.params.postId}`);

        this.setState({ post: postInfo.data, loading: false });
    }

    renderPostDetails = () => {
        const { loading, post } = this.state;

        if (loading) {
            return <Loader />
        } else {
            const { title, email, body } = post;
            return (
                <div className="ui items">
                    <div className="item">
                        <div className="content">
                            <h1>{title}</h1>
                            <div>{email}</div>
                            <div className="description">
                                <p>{body}</p>
                            </div>
                            <div className="extra">
                                <i className="green check icon"></i>
                                121 Votes
                        </div>
                        </div>
                    </div>
                </div>
            );
        }
    }

    render() {
        return (
            <React.Fragment>
                <Link to={`/users/${this.props.match.params.userId}`} className="ui labeled icon button">
                    <i className="left chevron icon"></i>
                    Volver
                </Link>
                {this.renderPostDetails()}
                <CommentList {...this.props} />
            </React.Fragment>
        )
    }
}

export default PostDetails;