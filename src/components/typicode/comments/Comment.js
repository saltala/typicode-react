import React from 'react'
import Anon from '../../../anon.png';
import { Link } from 'react-router-dom';

const Comment = (props) => {
    const { email, body } = props.commentInfo;

    return (
        <div class="comment" style={{ marginBottom: '1rem' }}>
            <div class="avatar">
                <img src={Anon} alt="User Avatar" />
            </div>
            <div class="content">
                <Link to={`/users/${props.match.params.userId}`} class="author">{email.toLowerCase()}</Link>
                <div class="text">
                    {body}
                </div>
                <div class="actions">
                    <a class="reply">Reply</a>
                    <a class="save">Save</a>
                    <a class="hide">Hide</a>
                    <a>
                        <i class="expand icon"></i>
                        Full-screen
                    </a>
                </div>
            </div>
        </div>
    )
}

export default Comment;