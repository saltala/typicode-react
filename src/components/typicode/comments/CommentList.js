import React, { Component } from 'react'
import axios from 'axios';

import Loader from '../layout/Loader';
import Comment from './Comment';

class CommentList extends Component {
    state = {
        comments: [],
        loading: false
    }

    componentDidMount() {
        this.getComments();
    }

    getComments = async () => {
        this.setState({ loading: true });

        const comments = await axios.get(`https://jsonplaceholder.typicode.com/comments?postId=${this.props.match.params.postId}`);

        this.setState({ comments: comments.data, loading: false });
    }

    renderComments = () => {
        if (this.state.loading) {
            return <Loader />
        } else {
            return (
                <React.Fragment>
                    <h2>Comentarios</h2>
                    <div class="ui comments">
                        {
                            this.state.comments.map((comment, index) => (<Comment {...this.props} commentInfo={comment} key={index} />))
                        }
                    </div>
                </React.Fragment>
            )
        }
    }

    render() {
        return (
            <div>
                {this.renderComments()}
            </div>
        )
    }
}

export default CommentList;
