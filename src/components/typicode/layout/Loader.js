import React from 'react'
import Loading from '../../../giphy.gif'

const Loader = () => {
    return (
        <React.Fragment>
            <img src={Loading} alt="Loading..." style={loaderStyle} />
        </React.Fragment>
    )
}

const loaderStyle = {
    width: '200px',
    display: 'block',
    margin: '0 auto'
}


export default Loader;