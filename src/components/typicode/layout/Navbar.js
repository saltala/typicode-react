import React from 'react'
import { Link } from 'react-router-dom';

function Navbar() {
    return (
        <div className="ui secondary pointing menu" style={{ marginBottom: '2rem' }}>
            <Link to="/users" className="item active">
                Usuarios
            </Link>
            <Link to="/rickandmorty" className="item">
                Rick & Morty
            </Link>
            <div className="right menu">
                <Link to="/logout" className="ui item">
                    Logout
                </Link>
            </div>
        </div>
    )
}

export default Navbar;