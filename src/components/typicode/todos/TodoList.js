import React from 'react'

import Todo from './Todo';

const TodoList = ({ todos }) => {

    const renderTodos = () => {
        return (<div class="ui segments">
            {todos.map((todo, index) => (
                <Todo key={index} todoInfo={todo} />
            ))}
        </div>)
    }

    return (
        <div>
            <h1>Todos</h1>
            {renderTodos()}
        </div>
    )
}

export default TodoList;
