import React from 'react'

const Todo = ({ todoInfo }) => {
    return (
        <div className="ui segment">
            <div className="ui checked checkbox">
                <input type="checkbox" checked={todoInfo.completed} />
                <label>{todoInfo.title}</label>
            </div>
        </div>
    )
}

export default Todo;
