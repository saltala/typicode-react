import React from 'react';
import { Link } from 'react-router-dom';

const User = ({ id, username, email, phone }) => {
    return (
        <tr>
            <td className="selectable" style={selectableIdStyles}>
                <Link to={`/users/${id}`}>{id}</Link>
            </td>
            <td>{username}</td>
            <td>{email}</td>
            <td>{phone}</td>
        </tr>
    )
}

const selectableIdStyles = {
    cursor: 'pointer',
    textAlign: 'center',
}

export default User;