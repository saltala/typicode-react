import React from 'react'
import axios from 'axios';
import { Link } from 'react-router-dom';
import PostList from '../posts/PostList';
import TodoList from '../todos/TodoList';

import Anon from '../../../anon.png'
import Loader from '../layout/Loader';

class UserDetail extends React.Component {
    state = {
        user: {},
        posts: [],
        todos: [],
        loading: false,
    }

    componentDidMount() {
        this.getUserData();
        this.getTodos();
    }

    getUserData = async () => {
        const id = this.props.match.params.id;

        this.setState({ loading: true });

        const user = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}`);
        const posts = await axios.get(`https://jsonplaceholder.typicode.com/posts?userId=${id}`)

        this.setState({ user: user.data, posts: posts.data, loading: false })
    }

    getTodos = async () => {
        const id = this.props.match.params.id;

        this.setState({ loading: true });
        const todos = await axios.get(`https://jsonplaceholder.typicode.com/todos?userId=${id}`);
        console.log(todos);
        this.setState({ todos: todos.data, loading: false });
    }

    renderUserInfo = () => {
        if (this.state.loading) {
            return <Loader />
        } else {
            const { id, name, username, email, phone, website } = this.state.user;

            return (
                <div className="ui raised segment">
                    <div className="ui relaxed divided items">
                        <div className="item">
                            <div className="ui small image circular avatar">
                                <img src={Anon} alt="User Avatar" />
                            </div>
                            <div className="content">
                                <div className="header">{name}</div>
                                <div className="description">
                                    <div className="ui segment">
                                        <p> id: {id}</p>
                                        <div className="ui divider"></div>
                                        <p>username: {username}</p>
                                        <div className="ui divider"></div>
                                        <p>email: {email}</p>
                                        <div className="ui divider"></div>
                                        <p>phone: {phone}</p>
                                        <div className="ui divider"></div>
                                        <p>website: {website}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }

    renderUserPosts = () => {
        if (this.state.posts && !this.state.loading) {
            return <PostList posts={this.state.posts} />
        }
    }

    renderUserTodos = () => {
        if (this.state.todos && !this.state.loading) {
            return <TodoList {...this.props} todos={this.state.todos} />
        }
    }

    render() {
        return (
            <React.Fragment>
                <Link to="/users" className="ui labeled icon button">
                    <i className="left chevron icon"></i>
                    Volver
                </Link>


                {this.renderUserInfo()}

                {this.renderUserTodos()}

                {this.renderUserPosts()}


            </React.Fragment >
        )
    }
}

export default UserDetail;

