import React from 'react';
import User from './User';
import Loader from '../layout/Loader';

const UserList = ({ users, loading }) => {
    const renderTable = () => {
        if (loading) {
            return <Loader />
        } else {
            return (
                <table className="ui teal table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Phone</th>
                        </tr>
                    </thead>
                    <tbody>
                        {renderUsers()}
                    </tbody>
                </table>
            )
        }
    }

    const renderUsers = () => {
        return users.map(user => {
            const { id, username, email, phone } = user;
            return <User key={id} id={id} username={username} email={email} phone={phone} />
        })
    }

    return (
        <React.Fragment>
            {renderTable()}
        </React.Fragment>
    )
}

export default UserList;