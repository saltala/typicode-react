import axios from 'axios';

const rickmorty = axios.create({
    baseURL: 'https://rickandmortyapi.com/api/',
});

export default rickmorty;